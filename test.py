

def payments():
    import requests

    headers = {
        # Already added when you pass json= but not when you pass data=
        'Content-Type': 'x-www-form-urlencoded',

    }

    json_data = {
        "secret": 'Secret Solextron Test',
        "pbtoken": '5af9919d52e418f4e1282cff9d34f107',
        "order":"122",
        "payment_link":"1",
        "product_name": 'Product Test (1 license)',
        "customer_name": 'José da Silva',
        "customer_taxid": '91051605962',
        "customer_email": 'spremjeet3@gmail.com',
        "customer_phone": '11 3328.9999',
        "address_street": 'Av.Paulista, 100',
        "address_zip": '01311100',
        "address_city": 'São Paulo',
        "address_state": 'SP',
        "amount_brl": '39.50',
        # "next_billing_date": '2022-08-10',
        # "frequency": 'M',
        # "response_type": "JSON"
    }
    # import pdb
    # pdb.set_trace();
    # response = requests.post('https://sandbox.pagbrasil.com/api/pagstream/subscription/add', headers=headers,
    #                          json=json_data)
    # response = requests.post('https://sandbox.pagbrasil.com/api/order/add', headers=headers,
    #                          json=json_data)
    import requests

    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
    }
    data ='secret=SecretPaymentTEST&pbtoken=5af9919d52e418f4e1282cff9d34f107&product_name=ProductTest&customer_name=José+da+Silva&customer_taxid=91051605962&customer_email=josedasilva@email.com.br&customer_phone=11+3328.9999&address_street=Av.Paulista,+100&address_zip=01311100&address_city=São+Paulo&address_state=SP&amount_brl=40&next_billing_date=2022-09-20&frequency=M'
    # data = 'secret=SecretPaymentTest&pbtoken=5af9919d52e418f4e1282cff9d34f107&subscription=soso3333&response_type=JSON'
    amount_brl = 21.10

    product_name = 'Assinatura Vinho (12un.) - Receba a cada 30 dias(Qty:1)'

    customer_name = 'test order'

    customer_taxid = 75989776233

    customer_email = "spremjeet3@gmail.com"

    customer_phone = '1146227411'

    address_street = "Marechal Emanuel Marques Porto"

    address_zip = "04855070"

    address_city = "São Paulo"

    address_state = "SP"
    post_fields = {

        'secret': 'SecretPaymentTEST',

        'pbtoken': '5af9919d52e418f4e1282cff9d34f107',

        'order': 142,
        "limit":0,
        "number_recurrences":0,

        'product_name': "Licença SOLEXTRON",

        'customer_name': customer_name,

        'customer_taxid': customer_taxid,

        'customer_email': customer_email,

        'customer_phone': customer_phone,

        'address_street': address_street,

        'address_zip': address_zip,

        'address_city': address_city,

        'address_state': address_state,

        'amount_brl': amount_brl,

        'frequency': 'W',

        'next_billing_date': '2022-08-18',
        "response_type":"JSON",

    }

    # data = 'secret=SecretPaymentTest&pbtoken=5af9919d52e418f4e1282cff9d34f107&product_name=Product+Test+(1+license)&customer_name=José+da+Silva&customer_taxid=91051605962&customer_email=spremjeet3@gmail.com&customer_phone=11+3328.9999&address_street=Av.Paulista,+100&address_zip=01311100&address_city=São+Paulo&address_state=SP&amount_brl=39.50&next_billing_date=2022-12-10&frequency=M'

    response = requests.post('https://sandbox.pagbrasil.com/api/pagstream/subscription/add', headers=headers, data=post_fields)
    import json
    print(json.loads(response.text),response.status_code)
# print(payments())
def addPaymentLink():
    import requests

    url = "https://sandbox.pagbrasil.com/api/order/add"
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
    }
    customer_name = 'test order'

    customer_taxid = 75989776233

    customer_email = "testes@testes"

    customer_phone = '1146227411'

    address_street = "Marechal Emanuel Marques Porto"

    address_zip = "04855070"

    address_city = "São Paulo"

    address_state = "SP"
    post_fields = {
                    'secret': 'SecretPaymentTEST',

                    'pbtoken': '5af9919d52e418f4e1282cff9d34f107',

                    'order':"150",

                    'payment_link': 1,

                    'product_name':"Licença SOLEXTRON",

        'customer_name': customer_name,

        'customer_taxid': customer_taxid,

        'customer_email': customer_email,

        'customer_phone': customer_phone,

        'address_street': address_street,

        'address_zip': address_zip,

        'address_city': address_city,

        'address_state': address_state,

                    'amount_brl': 500,

                    'payment_option': "C",
                  'frequency': 'M',

                    'url_return': 'https://pay.solextron.com/'}
    response = requests.post(url, headers=headers, data=post_fields)
    return response.text,response.status_code
print(addPaymentLink())
def get_order():
    import requests
    url = "https://sandbox.pagbrasil.com/api/order/get"
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
    }
    post_fields = {
                    'secret': 'SecretPaymentTEST',

                    'pbtoken': '5af9919d52e418f4e1282cff9d34f107',

                    'order':"125",
                   "response_type":"JSON",
    }
    response = requests.get(url, headers=headers,data=post_fields)
    import xmltodict,json
    xpars = xmltodict.parse(response.text)
    json_data = json.dumps(xpars)
    return json.loads(json_data)["request"]["order"]
# print(get_order())
def get_subscription():
    import requests
    url = "https://sandbox.pagbrasil.com/api/pagstream/subscription/get"
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
    }
    post_fields = {
        'secret': 'SecretPaymentTEST',

        'pbtoken': '5af9919d52e418f4e1282cff9d34f107',

        'subscription': 143,
        "response_type": "JSON",
    }
    response = requests.post(url, headers=headers, data=post_fields)
    # import xmltodict, json
    # xpars = xmltodict.parse(response.text)
    # json_data = json.dumps(xpars)
    return response.text
# print(get_subscription())
